#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
import scipy.special as sc
from mpl_toolkits import mplot3d
from matplotlib import cm


n=30

def surf(m,l,p, t): #p: phi, t: theta
    R=np.absolute(sc.sph_harm(m,l,p,t))**2
    x=R*np.sin(t)*np.cos(p)
    y=R*np.sin(t)*np.sin(p)
    z=R*np.cos(t)
    return x,y,z

def plotorbital(l,m):
    t=np.linspace(0,np.pi,n)
    p=np.linspace(0,2*np.pi,2*n)
    Rmax=np.absolute(np.max(sc.sph_harm(m,l,0,t)*np.conj(sc.sph_harm(m,l,0,t))))
    m_p, m_t=np.meshgrid(p,t)
    x,y,z= surf(m,l,m_p,m_t)
    ax1 = plt.axes(projection ='3d')
    ax1.plot_surface(x, y, z,cmap=cm.jet, linewidth=0.1)
    ax1.set_xlim3d(-Rmax,Rmax)
    ax1.set_ylim3d(-Rmax,Rmax)
    ax1.set_zlim3d(-Rmax,Rmax)
    plt.show()


while True:
    print("Drehimuplszahl eingeben:")
    l = int(input())
    print("Magnetische Quantenzahl eingeben:")
    m = int(input())
    if m>l or m < -l:
        print("Physikalisch nicht mögliche Kombination!")
        continue
    plotorbital(l,m)
