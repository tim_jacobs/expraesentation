#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt

t=np.arange(0,2 *np.pi,0.001)
p=15/(8 *np.pi)*np.cos(t)**2*np.sin(t)**2
plt.polar(t,p)
plt.show()
